// import {binance} from "binance-node-api";

import {Double} from "bson";
import * as moment from "moment";
import _date = moment.unitOfTime._date;

const binance = require('node-binance-api');

export class Binance {
    configure(): void {
        binance.options({
            'APIKEY'   : 'h8o7ob2qRmNTzHJjKaG6LuQEJpPIyA5keU7OwGC4ZTtgjakgtQJinhOeoqsV3NQE',
            'APISECRET': 'NR821BiM8m2gMIEpy7kVV2SkdbSGcZvqfYwJugORP6XV0B1HyqUhDjNVAadGJ62Y',
            'test'     : false
        });
    }


    async fetchPair(pair: String): Promise<PulseData> {
        return new Promise<PulseData>((resolve, reject) => {
            let pulseDate = new Date();
            binance.depth(pair, (error, orderBook, symbol) => {
                if (error) {
                    console.log("Binance fetchPair(" + symbol + ") error: " + error);
                    reject();
                }

                let noRecords = !orderBook || !orderBook.bids || !orderBook.asks;
                if (noRecords)
                {
                    console.log('Binance error: NO RECORD ');
                    reject();
                }

                let asks = this.extruct(orderBook.asks);
                let bids = this.extruct(orderBook.bids);
                let result = new PulseData(pulseDate, asks, bids);

                resolve(result);
            });
        });
    }

    extruct(ticks: Object, limit: number = 20): {price: string, amount: number}[] {
        let keys = Object.keys(ticks).slice(0, limit);
        return keys.map( (priceKey: string) => ({price: priceKey, amount: ticks[priceKey]}));
    }
}

export class PulseData {
    date: Date;
    asks: {price: string, amount: number}[];
    bids: {price: string, amount: number}[];

    constructor(date: Date, asks: {price: string, amount: number}[], bids: {price: string, amount: number}[]) {
        this.date = date;
        this.asks = asks;
        this.bids = bids;
    }
}