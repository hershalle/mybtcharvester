import * as Sequelize from "sequelize";

export class MYSQL {

    constructor() {}

    private database = "BTCUSD";
    private username = "rotemx";
    private password = "BTCIsTheKing!";

    private sequelize = new Sequelize(this.database, this.username, this.password, {
        host: '127.0.0.1',
        dialect: 'mysql',

        pool: {
            max    : 5,
            min    : 0,
            acquire: 30000,
            idle   : 10000
        },
    });

    private asksTableName = 'asks';
    private asksTableRecord = this.sequelize.define(this.asksTableName, {
        pulse: {
            type: Sequelize.TIME
        },
        price: {
            type: Sequelize.STRING
        },
        amount: {
            type: Sequelize.STRING
        }
    });

    async authenticate() {
        return await this.sequelize.authenticate().
        then(() => {
            console.log('MYSQL connection has been established successfully.');
        }).catch(err => {
            console.error('MYSQL authenticate failed: ', err);
        });
    }

    async addAskRecord(askRecord: {price: string, amount: string}) {
        this.asksTableRecord.sync().then(() => {
            return this.asksTableRecord.create({
                pulse: new Date(),
                price: askRecord.price,
                amount: askRecord.amount
            })
        })
    }
}
